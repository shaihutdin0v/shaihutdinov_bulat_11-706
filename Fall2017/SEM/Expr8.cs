﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class Program
    {
        static void Main()
        {
            Console.WriteLine("Введите a, b, c уравнения прямой");
            Console.Write("a = ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("b = ");
            double b = double.Parse(Console.ReadLine());
            Console.Write("c = ");
            double c = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите (x0; y0) определненной точки");
            Console.Write("x0 = ");
            double x0 = double.Parse(Console.ReadLine());
            Console.Write("y0 = ");
            double y0 = double.Parse(Console.ReadLine());

            double x, y;
            if (a != 0)
            {
                y = (a * a * y0 - a * b * x0 - b * c) / (a * a + b * b);
                x = -(b * y + c) / a;
            }
            else
            {
                x = x0;
                y = -c / b;
            }
            Console.WriteLine("Точка пересечения: (" + x + "; " + y + ")");
            Console.ReadKey();
        }
    }

