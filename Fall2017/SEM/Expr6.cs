﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите х1, у1 первой точки прямой:");
            Console.WriteLine("Введите х1:");
            int x1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Введите у1:");
            int y1 = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите х2, у2 второй точки прямой:");
            Console.WriteLine("Введите x2:");
            int x2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Введите y2:");
            int y2 = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите х0, у0 произвольной точки");
            Console.WriteLine("Введите х0:");
            int x0 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Введите y0:");
            int y0 = Int32.Parse(Console.ReadLine());

            double distance = Math.Abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / Math.Sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
            Console.WriteLine(distance);
        }
    }
}
