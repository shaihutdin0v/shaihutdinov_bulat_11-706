﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            int number, k, sum;
            sum = 0;
            number = Int32.Parse(Console.ReadLine());
            for (k = 2; k <= number / 2; k++)
            {
                if (number % k == 0)
                    sum = sum + k;
            }
            Console.WriteLine(sum);
        }
    }
}
