﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            int znak = 0; int n;
            n = Int32.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Random rand = new Random();
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(-100, 100);
            for (int i = 0; i < arr.Length-1; i++)
            {
                if (arr[i] * arr[i + 1] >= 0)
                    znak = 0;
            }
            if (znak == 1) Console.WriteLine("Является знакопеременной");
            else Console.WriteLine("Не является знакопеременной");
        }
    }
}
