﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class Program
    {
        static void Main(string[] args)
        {
            string a;
            int[] b = new int[100];
            int i = 0;
            while ((a = Console.ReadLine()) != "0")
                b[i++] = Convert.ToInt32(a);
            for (int k = 0; k < i; k++)
                Console.Write(b[k] + " ");
            int max = b.Max();
            Console.WriteLine("Наибольшее число:"+ max);
            int min = b.Min();
            Console.WriteLine("Наименьшее число:" + min);
        }
    }
}
