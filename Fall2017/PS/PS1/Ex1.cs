﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            int year = int.Parse(Console.ReadLine());
            if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
            {
                if (year < 10) Console.WriteLine("12/09/000" + year);
                if (year < 100 && year >= 10) Console.WriteLine("12/09/00" + year);
                if (year < 1000 && year >= 100) Console.WriteLine("12/09/0" + year);
                if (year >= 1000) Console.WriteLine("12/09/" + year);
            }
            else
            {
                if (year < 10) Console.WriteLine("12/09/000" + year);
                if (year < 100 && year >= 10) Console.WriteLine("12/09/00" + year);
                if (year < 1000 && year >= 100) Console.WriteLine("12/09/0" + year);
                if (year >= 1000) Console.WriteLine("12/09/" + year);
            }

        }
    }
}
